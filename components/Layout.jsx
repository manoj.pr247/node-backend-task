import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import Navbar from './Navbar';

const Layout = ({ children }) => (
  <div>
    <Head>
      <script src="https://code.jquery.com/jquery-3.1.1.min.js" crossOrigin="anonymous" />
      <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css"
      />
      <script src="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.js" />
    </Head>
    <Navbar />
    <br />
    <br />
    <br />
    <div className="ui container">
      {children}
    </div>
  </div>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
