import React from 'react';

const Navbar = () => (
  <div className="ui top fixed menu">
    <div className="item">
      Rydeu Corporate
    </div>
    <a href="/" className="item">Dashboard</a>
    <a href="/create_order" className="item">Create Order</a>
  </div>
);

export default Navbar;
