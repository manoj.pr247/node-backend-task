const development = {
  use_env_variable: false,
  database: 'rydeu_db',
  username: 'rydeu',
  password: 'rydeu@123',
  databaseConfigs: {
    hostname: 'localhost',
    dialect: 'postgres',
  },
};

const production = {
  use_env_variable: false,
  database: '',
  username: '',
  password: '',
  databaseConfigs: {
    hostname: '',
    dialect: 'postgres',
  },
};


module.exports = {
  development,
  production,
};
