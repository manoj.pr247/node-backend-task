const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
  res.render(req, res, '/', req.query);
});

module.exports = router;
