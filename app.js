const express = require('express');
const next = require('next');

const debug = require('debug')('rydeu-backend:server');
const http = require('http');

const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const { ApolloServer } = require('apollo-server-express');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');

const typeDefs = require('./graphQl/schemas');
const resolvers = require('./graphQl/resolvers');

const models = require('./models/Models');


const dev = process.env.NODE_ENV !== 'production';


// Next.js configs
const app = next({ dev });
const handle = app.getRequestHandler();

let nodeServer = null;

// server setup helpers
/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string'
    ? `Pipe ${port}`
    : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const addr = nodeServer.address();
  const bind = typeof addr === 'string'
    ? `pipe ${addr}`
    : `port ${addr.port}`;
  debug(`Listening on ${bind}`);
}


app.prepare().then(() => {
  const server = express();
  server.use(logger('dev'));
  server.use(express.json());
  server.use(express.urlencoded({ extended: false }));
  server.use(cookieParser());
  server.use(express.static(path.join(__dirname, 'public')));

  const graphQlServer = new ApolloServer({
    // These will be defined for both new or existing servers
    typeDefs,
    resolvers,
    context: ({ req }) => ({ request: req, models }),
    playground: process.env.NODE_ENV !== 'production',
  });

  graphQlServer.applyMiddleware({ app: server });

  server.use('/api', indexRouter);
  server.use('/api/users', usersRouter);

  server.get('*', (req, res) => handle(req, res));
  const port = normalizePort(process.env.PORT || '3000');
  server.set('port', port);

  /**
   * Create HTTP server.
   */

  nodeServer = http.createServer(server);

  /**
   * Listen on provided port, on all network interfaces.
   */

  nodeServer.listen(port);
  nodeServer.on('error', onError);
  nodeServer.on('listening', onListening);
}).catch((ex) => {
  console.error(ex.stack);
  process.exit(1);
});
