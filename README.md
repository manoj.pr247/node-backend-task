1. Build a api for creating deal
2. use auth module to validate user
3. find user and org id then build the deal using a service
4. User proper validation to validat params
5. serialize deal response and return to frontend


Booker_id (user id) mandatory
Start_date_time   presence: true mandatory
End_date_time  presence: true mandatory
Organization_id presence: true mandatory
Confirmation_date (confirmed by client) optional
Expires_at (client does not confirmed optional
Create_at optional
Updated_at optional
Total number of pax: mandatory
