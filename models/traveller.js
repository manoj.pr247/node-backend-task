const Sequelize = require('sequelize');
const Model = Sequelize.Model;

const databaseConnection = require('./index');
//const User = require('./user');

const Traveller = databaseConnection.define('Traveller', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            unique: true,
            primaryKey: true
        },
        company_name: {
            type: Sequelize.STRING
        },
        organization_id: {
            type: Sequelize.STRING
        }
    });

//Traveller.hasOne(User);

module.exports = Traveller;