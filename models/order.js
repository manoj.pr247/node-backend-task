const Sequelize = require('sequelize');
const Model = Sequelize.Model;

const databaseConnection = require('./index');
const MasterOrder = require('./masterOrder');
const Traveller = require('./traveller');
const Product = require('./product');
const Booker = require('./booker');

const Order = databaseConnection.define('Order',{
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        unique: true
    },
    order_id: {
        type: Sequelize.STRING

    },
    completed_date: {
        type: Sequelize.DATE
    },
    order_status: {
        type: Sequelize.STRING
    },
    start_lat: {
        type: Sequelize.STRING
    },
    start_long: {
        type: Sequelize.STRING
    },
    end_lat: {
        type: Sequelize.STRING
    },
    end_long: {
        type: Sequelize.STRING
    },
    total_price: {
        type: Sequelize.FLOAT(4)
    },
    base_price: {
        type: Sequelize.FLOAT(4)
    },
    tax: {
        type: Sequelize.FLOAT(4)
    },
    pickup_time:{
        type: Sequelize.DATE
    },
    drop_time: {
        type: Sequelize.DATE
    },
    flight_number: {
        type: Sequelize.STRING
    },
    drop_hotel_name: {
        type: Sequelize.STRING
    },
    pickup_city: {
        type: Sequelize.STRING
    },
    pickup_country:{
        type: Sequelize.STRING
    },
    drop_city: {
        type: Sequelize.STRING
    },
    drop_country: {
        type: Sequelize.STRING
    },
    pickup_address: {
        type: Sequelize.STRING
    },
    drop_address: {
        type: Sequelize.STRING
    },
    pickup_area: {
        type: Sequelize.STRING
    },
    drop_area: {
        type: Sequelize.STRING
    },
    pickup_pin: {
        type: Sequelize.STRING
    },
    drop_pin: {
        type: Sequelize.STRING
    }
});

Order.belongsTo(MasterOrder);
Order.hasMany(Traveller);
Order.hasOne(Product);
Order.hasOne(Booker);

module.exports = Order;