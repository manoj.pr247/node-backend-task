const User = require('./user');
const Booker = require('./booker');
const Traveller = require('./traveller');

Traveller.hasOne(User);
Booker.hasMany(Traveller);
Booker.hasOne(User);

let db = {
    Booker,
    Traveller,
    User
};


module.exports = db;