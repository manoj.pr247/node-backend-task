const Sequelize = require('sequelize');
const Model = Sequelize.Model;

const databaseConnection = require('./index');
const Unit = require('./unit');

const Product = databaseConnection.define('Product', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        unique: true,
        primaryKey: true
    },
    product_name: {
        type: Sequelize.STRING
    },
    product_details: {
        type: Sequelize.STRING
    },
    product_base_price: {
        type: Sequelize.FLOAT(4)
    },
    price_per_unit: {
        type: Sequelize.FLOAT(4)
    }
});

Product.hasOne(Unit);

module.exports = Product;