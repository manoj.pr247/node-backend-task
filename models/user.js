const Sequelize = require('sequelize');
const Model = Sequelize.Model;

const databaseConnection = require('./index');


const User = databaseConnection.define('User', {
     "first_name": {
         type: Sequelize.STRING
     },
     "last_name": {
         type: Sequelize.STRING
     },
     "company_name": {
         type: Sequelize.STRING
     },
    "organization_id": {
         type: Sequelize.INTEGER
    },
     "business_email": {
         type: Sequelize.STRING
     },
     "mobile_no": {
         type: Sequelize.STRING
     },
     "office_phone": {
         type: Sequelize.STRING
     },
     "terms_and_condition": {
         type: Sequelize.STRING
     },
     "id": {
         type: Sequelize.INTEGER,
         autoIncrement: true,
         unique: true,
         primaryKey: true
     }
});


module.exports = User;