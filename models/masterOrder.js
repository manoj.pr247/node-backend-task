const Sequelize = require('sequelize');
const Model = Sequelize.Model;

const databaseConnection = require('./index');
const Order = require('./order');
const Booker = require('./booker');

const MasterOrder = databaseConnection.define('MasterOrder', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        unique: true
    },
    order_id: {
        type: Sequelize.STRING

    },
    completed_date: {
        type: Sequelize.DATE
    },
    order_status: {
        type: Sequelize.STRING
    },
    total_price: {
        type: Sequelize.FLOAT(4)
    },
    currency: {
        type: Sequelize.STRING
    }
});


MasterOrder.hasMany(Order);
MasterOrder.hasOne(Booker);

module.exports = MasterOrder;