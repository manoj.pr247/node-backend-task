const Sequelize = require('sequelize');

const databaseConnection = require('./index');
// const User = require('./user');
// const Traveller = require('./traveller');

const Booker = databaseConnection.define('Booker',{
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        unique: true,
        primaryKey: true
    },
    company_name: {
        type: Sequelize.STRING
    },
    organization_id: {
        type: Sequelize.INTEGER
    }
});


// Booker.hasOne(User);
// Booker.hasMany(Traveller);

module.exports = Booker;