const Sequelize = require('sequelize');
const env       = process.env.NODE_ENV || 'development';
const config    = require('../config/config.js')[env];

let sequelize = new Sequelize(config.database, config.username, config.password, config.databaseConfigs);

module.exports = sequelize;
