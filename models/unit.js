const Sequelize = require('sequelize');
const Model = Sequelize.Model;

const databaseConnection = require('./index');


const Unit = databaseConnection.define('Unit', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        unique: true
    },
    unit_name: {
        type: Sequelize.STRING
    },
    unit_currency: {
        type: Sequelize.STRING
    },
    unit_conversion_factor: {
        type: Sequelize.INTEGER
    },
    unit_description: {
        type: Sequelize.STRING
    }
});

module.exports = Unit;