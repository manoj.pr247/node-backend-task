import React from 'react';
import Layout from '../components/Layout';

const CreateOrder = () => (
  <Layout>
    <div className="ui two column centered grid">
      <div className="column">
        <h1>Create an order</h1>
        <form className="ui form">
          <div className="field">
            <label>Base Price</label>
            <input type="number" name="" placeholder="" />
          </div>
          <div className="field">
            <label>Total Price</label>
            <input type="number" name="" placeholder="" />
          </div>
          <div className="field">
            <label>Tax</label>
            <input type="number" name="" placeholder="" />
          </div>
          <div className="field">
            <label>Flight Number</label>
            <input type="text" name="" placeholder="" />
          </div>
          <div className="field">
            <label>Hotel Name</label>
            <input type="text" name="" placeholder="" />
          </div>
          <br />
          <h4 className="ui dividing header">Pickup</h4>
          <div className="field">
            <label>Pickup Time</label>
            <input type="datetime-local" name="" placeholder="" />
          </div>
          <div className="field">
            <label>Address</label>
            <input type="text" name="" placeholder="" />
          </div>
          <div className="field">
            <label>Area</label>
            <input type="text" name="" placeholder="" />
          </div>
          <div className="field">
            <label>City</label>
            <input type="text" name="" placeholder="" />
          </div>
          <div className="field">
            <label>Country</label>
            <input type="text" name="" placeholder="" />
          </div>
          <div className="field">
            <label>Pincode</label>
            <input type="text" name="" placeholder="" />
          </div>
          <br />
          <h4 className="ui dividing header">Drop</h4>
          <div className="field">
            <label>Drop Time</label>
            <input type="datetime-local" name="" placeholder="" />
          </div>
          <div className="field">
            <label>Address</label>
            <input type="text" name="" placeholder="" />
          </div>
          <div className="field">
            <label>Area</label>
            <input type="text" name="" placeholder="" />
          </div>
          <div className="field">
            <label>City</label>
            <input type="text" name="" placeholder="" />
          </div>
          <div className="field">
            <label>Country</label>
            <input type="text" name="" placeholder="" />
          </div>
          <div className="field">
            <label>Pincode</label>
            <input type="text" name="" placeholder="" />
          </div>
          <button className="ui button" type="submit">Submit</button>
        </form>
        <br />
        <br />
      </div>
    </div>
  </Layout>
);

export default CreateOrder;
