import {useGraphQL} from 'graphql-react';
import Layout from '../components/Layout';

export default () => {
    const {loading, cacheValue = {}} = useGraphQL({
        fetchOptionsOverride(options) {
            options.url = 'https://graphql-pokemon.now.sh'
        },
        operation: {
            query: /* GraphQL */ `
        {
          pokemon(name: "Pikachu") {
            name
            image
          }
        }
      `
        }
    });

    const {data} = cacheValue;
    return data ? (
        <Layout>
        <div><p>order details page from server</p><img src={data.pokemon.image} alt={data.pokemon.name}/></div>
        </Layout>

    ) : loading ? (
        <p>Loading…</p>
    ) : (
        <p>Error!</p>
    )
}

